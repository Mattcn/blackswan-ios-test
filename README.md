# BlackSwan iOS Job Application Test

**Please complete the test in Swift.**

We want to see how clean your code is!

Please write an application that uses public API's to fetch data and display it in an organised way. 

Don't over engineer it, we don't want to take up all your day. 

**Example API's:**

| Site   |      URL      | API Key |
|:----------|:-------------|:-----:|
| Giphy |  https://github.com/Giphy/GiphyAPI | - |
| UK Police |    https://data.police.uk/docs/   |  - |
| StackExchange | https://api.stackexchange.com/docs | - | 
| JSON Placeholder | http://jsonplaceholder.typicode.com | - |
| Open Weather | http://openweathermap.org/api | - | 
| TFL | https://api.tfl.gov.uk/ | - | 
| Transport for Budapest | http://docs.bkkfutar.apiary.io/ | - |
| The Movie DB | http://docs.themoviedb.apiary.io/ | 0a08e38b874d0aa2d426ffc04357069d | 

...or surprise us. 

We could be sneaky and not say anything else, but here's some things we're looking to for:

* Use of existing open source libraries
* Unit Tests and/or UI Tests 
* Clean code and project structure
* Brownie points for dependancy injection

### Submission notes

You can just submit a PR here, create a private repo for free on [GitLab](https://www.gitlab.com/?gclid=CLCBmaWM474CFaMSwwodAqIAqw) or [Bitbucket](https://bitbucket.org/), or just send us the repo by email. Whatever you prefer.

---

[@BlackSwan](https://www.bkakswan.com) - 2016
